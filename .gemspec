Gem::Specification.new do |s|
  s.name = "zbxapi"
  s.rubyforge_project = "zbxapi"
  s.version = "0.3.99"
  s.authors = ["A. Nelson"]
  s.email = %q{nelsonab@red-tux.net}
  s.summary = %q{Ruby wrapper to the Zabbix API}
  s.homepage = %q{https://github.com/red-tux/zbxapi}
  s.description = %q{Provides a straight forward interface to manipulate Zabbix servers using the Zabbix API.}
  s.licenses = "LGPL 2.1"
  s.requirements = "Requires json"
  s.add_dependency('json')
  s.require_paths =["."]
  s.files =
    ["LICENSE", "zbxapi.rb", "zbxapi/zdebug.rb", "zbxapi/api_exceptions.rb",
     "zbxapi/exceptions.rb", "zbxapi/utils.rb", "zbxapi/result.rb",
     "api_classes/api_dsl.rb",
#     "zbxapi/revision.rb",
     Dir["api_classes/dsl*.rb"]].flatten
end

